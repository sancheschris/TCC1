package sistema.entidades;

import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;


@Entity
public class Usuario {

	
	private String email;
	private String nome;
	private Date dataNascimento;
	//@OneToMany(cascade=CascadeType.ALL, mappedBy = "usuario")
	@Transient
	private ArrayList<Equipe> equipes = new ArrayList<Equipe>();
	//@OneToMany(cascade=CascadeType.ALL, mappedBy = "usuario")
	@Transient
	private ArrayList<Campeonato> campeonatos = new ArrayList<Campeonato>();
	private String telefoneFixo;
	private String telefoneMovel;
	private String endereco;
	private String rg;
	@Id
	private String cpf;
	private String sexo;

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public ArrayList<Equipe> getEquipes() {
		return equipes;
	}
	public void setEquipes(ArrayList<Equipe> equipes) {
		this.equipes = equipes;
	}
	public ArrayList<Campeonato> getCampeonatos() {
		return campeonatos;
	}
	public void setCampeonatos(ArrayList<Campeonato> campeonatos) {
		this.campeonatos = campeonatos;
	}
	public String getTelefoneFixo() {
		return telefoneFixo;
	}
	public void setTelefoneFixo(String telefoneFixo) {
		this.telefoneFixo = telefoneFixo;
	}
	public String getTelefoneMovel() {
		return telefoneMovel;
	}
	public void setTelefoneMovel(String telefoneMovel) {
		this.telefoneMovel = telefoneMovel;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public void addEquipes(Equipe equipe)
	{
		equipes.add(equipe);
	}
	
	public void addCampeonatos(Campeonato campeonato)
	{
		campeonatos.add(campeonato);
	}
	

}
