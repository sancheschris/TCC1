package sistema.entidades;

public class Cartao {

	private String amarelo;
	private String vermelho;
	
	public Cartao() {
		super();
	}
	public String getAmarelo() {
		return amarelo;
	}
	public void setAmarelo(String amarelo) {
		this.amarelo = amarelo;
	}
	public String getVermelho() {
		return vermelho;
	}
	public void setVermelho(String vermelho) {
		this.vermelho = vermelho;
	}
	
	
}
